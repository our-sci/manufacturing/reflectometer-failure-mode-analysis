# Feature Prioritization for OpenTEAM Tech Development

This is the second iteration of our community-governed feature prioritization running list. The goal is to have a list of unprioritizated feature requests, updated weekly. We will then poll our community to rank feature requests by priority on a quarterly basis.


* [**Feature Requests**](https://openteamag.gitlab.io/feedback/feature-prioritization/feature_ranking_table.html) - a sheet of requests that allows our community to track unranked and ranked feature requests.


The worksheet is written as CSV files are validated against a JSON schema. This allows simple interaction with the data via spreadsheet software, but also allows us to check the data integrity and check the files into Git as text for posterity.

In case of schema failure, the table won't be replaced until the data is fixed back into compliancy, and a table called "errors_table" will be stored between the CI artifacts, so the admin can deploy a fix.
